
<!DOCTYPE html>
<html lang="en">
	<head>

		<title></title>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="robots" content="" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

			<script src="sleepysleep/vendor/jquery/dist/jquery.js" type="text/javascript"></script>
			<script src="sleepysleep/vendor/jquery.tie/jquery.tie.js" type="text/javascript"></script>
			<script src="sleepysleep/js/main.min.js" type="text/javascript"></script>
			<link href="sleepysleep/styles/main.css" rel="stylesheet">
	</head>
	<body class="sleepysleep">

	
	<?include('.snippets/header.php');?>
<div class="beds-gallery">
	<figure class="figure-1" id="figure-1">
		
			<img src="sleepysleep/images/beds/gallery/1.jpg" alt="" />
			<video  id="header-video" loop="" autoplay autoload="autoload" poster="sleepysleep/video/video-poster.jpg" style="display: block; opacity:0;">
			</video>
			<script>
			!(function(videotag, wrapper) {
				var formats = [
					{
						ext: 'ogv',
						type: "video/ogg; codecs='theora, vorbis'"
					},
					{
						ext: 'mp4',
						type: "video/mp4;"
					},
					{
						ext: 'webm',
						type: "video/webm;"
					}
				];
				$(videotag)
				.tie(function() {
					for (var i=0;i<formats.length;i++) {
						var format = formats[i];
						$(this).put($('<source />', {
							"src": "sleepysleep/video/"+window.videoRatio+"."+format.ext+"?rand="+Math.random(),
							"type": format.type
						}));
					};
				});
				videotag.load();
				
				/* Now we must trim wrapper by aspect ratio 0.5 and wrapper by 0,32291666666666666666666666666667*/
				var trimVideo = function() {
					// Trim video
					var wrapW = $(videotag).parent().width();
					var mL = ($(videotag).width()-$(videotag).parent().width())/-2;
					
					$(videotag).css({
						
						"margin-left": mL+'px'
					});
					// Trim video
				}
				$(window).resize(function() {
					trimVideo();
				});
				trimVideo();

				/*
				Show video slowly
				*/
				var displayWrapper = function() {
					$(wrapper).animate({
						opacity:1
					},1000);
				}
				var displayVideo = function(ok) {
					displayWrapper();
					if (ok||false) {
						$(videotag).animate({
							opacity:1
						},1000);
						videotag[0].play();
					};
				};

				var testWait, waitLimit, protect = 600;
				var testVideo = function() {	
					protect--;
					if (protect<=0) return;
					if (!waitLimit) return;
					console.log(videotag[0].readyState);
					if (videotag[0].readyState!=4) {
						setTimeout(function() {
							testVideo();
						},50);
					} else {
						
						if (waitLimit) clearTimeout(waitLimit);
						displayVideo(true);
					};
				};
				var waitLimit = setTimeout(function() {
					
					displayVideo();
				}, 750);
				testVideo();

			})($("#header-video"), $("#header-video-wrapper"));
	</script>
			<figcaption>
				<div class="bg"></div>
				<div class="caption"><a></a>
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
				</div>
			</figcaption>
		
	</figure>
	<figure class="figure-2" id="figure-2">
		
			<img src="sleepysleep/images/beds/gallery/2.jpg" alt="">
			<figcaption> 
				<div class="bg"></div>
				<div class="caption">
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
				</div>
			</figcaption>
		
	</figure>
	<figure class="figure-3" id="figure-3">
		
			<img src="sleepysleep/images/beds/gallery/3.jpg" alt="">
			<figcaption> 
				<div class="bg"></div>
				<div class="caption">
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
				</div>
			</figcaption>
		
	</figure>
	<figure class="figure-4" id="figure-4">
		
			<img src="sleepysleep/images/beds/gallery/4.jpg" alt="">
			<figcaption> 
				<div class="bg"></div>
				<div class="caption">
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
				</div>
			</figcaption>
		
	</figure>
	<figure class="figure-5" id="figure-5">
		
			<img src="sleepysleep/images/beds/gallery/5.jpg" alt="">
			<figcaption> 
				<div class="bg"></div>
				<div class="caption">
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
				</div>
			</figcaption>
		
	</figure>
	<figure class="figure-6">
		
			<img src="sleepysleep/images/beds/gallery/6.jpg" alt="">
			<figcaption> 
				<div class="bg"></div>
				<div class="caption">
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
				</div>
			</figcaption>
		
	</figure>
	<figure class="figure-7">
		
			<img src="sleepysleep/images/beds/gallery/7.jpg" alt="">
			<figcaption> 
				<div class="bg"></div>
				<div class="caption">
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
				</div>
			</figcaption>
		
	</figure>
	<figure class="figure-8">
		
			<img src="sleepysleep/images/beds/gallery/8.jpg" alt="">
			<figcaption> 
				<div class="bg"></div>
				<div class="caption">
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
					
				</div>
			</figcaption>
		
	</figure>
	<figure class="figure-9">
		
			<img src="sleepysleep/images/beds/gallery/9.jpg" alt="">
			<figcaption> 
				<div class="bg"></div>
				<div class="caption">
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
				</div>
			</figcaption>
		
	</figure>
	<figure class="figure-10">
		
			<img src="sleepysleep/images/beds/gallery/10.jpg" alt="">
			<figcaption> 
				<div class="bg"></div>
				<div class="caption">
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
				</div>
			</figcaption>
		
	</figure>
	<figure class="figure-11">
		
			<img src="sleepysleep/images/beds/gallery/11.jpg" alt="">
			<figcaption> 
				<div class="bg"></div>
				<div class="caption">
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
				</div>
			</figcaption>
		
	</figure>
	<figure class="figure-12">
		
			<img src="sleepysleep/images/beds/gallery/12.jpg" alt="">
			<figcaption> 
				<div class="bg"></div>
				<div class="caption">
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
				</div>
			</figcaption>
		
	</figure>
	<figure class="figure-13">
		
			<img src="sleepysleep/images/beds/gallery/13.jpg" alt="">
			<figcaption> 
				<div class="bg"></div>
				<div class="caption">
					<h3>Шератон</h3>
					<summary>Кровать «Шератон» - настоящая находка для страстных романтиков и приверженцев классического стиля в интерьере. «Шератон» создает комфорт и способствует спокойному отдыху и неге.</summary>
					<a href=""><button>Подробнее</button></a>
				</div>
			</figcaption>
		
	</figure>
</div>
<script>
	;(function() {

		var recheckers = [];
		var figures = {};
		var complexMap = {
			"figure-4": {
				"listenHeightGroup": {join:["figure-2"],to:"figure-1"}
			},
			"figure-5": {
				"listenHeight": "figure-4"
			},
			"figure-3": {
				"listenHeight": "figure-2"
			}
		};
		var recalc = function() {
			console.log('recalc');
			$.each(complexMap, function(obj, data) {
					figures[obj].pureHeight = figures[obj].node.width*figures[obj].ratio;
					$(figures[obj].node).attr("pure", figures[obj].pureHeight);
					$.each(data, function(method, rel) {
						
						
						switch(method) {
							case "listenHeight": 
								figures[obj].node.height = figures[rel].node.height;
							break;
							case 'listenHeightGroup':

								figures[obj].node.height = figures[obj].node.height + (figures[rel.to].node.height - (figures[rel.join].node.height+figures[obj].node.height));
							break;
						}
					});
			});
		}
		
		var loadings=0;
		var loaded = function() {
			loadings--;
			if (loadings===0) {
				$(window).resize(function() {
					recalc();
				});
				recalc();
			}
		};
		$("figure").each(function() {
				loadings++;
				var image = $(this).find('img')[0];
				var id = $(this).attr("id");
				figures[id] = {
					node: image
				};
				
				var newImage = new Image();
				newImage.onload = function() {
					var ratio = this.height/this.width;
					figures[id].ratio = ratio;
					var recheck = function() {
						figures[id].pureHeight = image.width*ratio;
						$(figures[id].node).attr("pure", figures[id].pureHeight);
						$(figures[id].node).attr("ratio", figures[id].ratio);
						loaded();
					};
					recheck();
					recheckers.push(recheck);
				};
				newImage.src = $(image).attr("src");
		});

	})();
</script>
<div class="limit clear footer">
	<article>Sleepeesleep – бренд группы компаний Estetica, признанного одним из лучших производителей ортопедических матрасов и входящего в тройку крупнейших российских производителей товаров для сна. Вся продукция создается из сертифицированных гипоаллергенных материалов.</article>
	<div class="copyrights-pro">
		<a href=""><img src="sleepysleep/images/fsoc_vk.png" alt="vk" /></a>
		<a href=""><img src="sleepysleep/images/fsoc_tw.png" alt="vk" /></a>
		<a href=""><img src="sleepysleep/images/fsoc_fb.png" alt="vk" /></a>
		<summary>© 2015 группа компаний «эстетика»</summary>
	</div>
</div>


	</body>
<html> 