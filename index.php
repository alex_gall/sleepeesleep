
<!DOCTYPE html>
<html lang="en">
	<head>

		<title></title>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="robots" content="" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

			<script src="sleepysleep/vendor/jquery/dist/jquery.js" type="text/javascript"></script>
			<script src="sleepysleep/vendor/jquery.tie/jquery.tie.js" type="text/javascript"></script>
			<script src="sleepysleep/js/main.min.js" type="text/javascript"></script>
			<link href="sleepysleep/styles/main.css" rel="stylesheet">
	</head>
	<body class="sleepysleep">

	
	<?include('.snippets/header.php');?>

	<div class="video" id="header-video-wrapper">
	<video  id="header-video" loop="" autoplay autoload="autoload" poster="sleepysleep/video/video-poster.jpg" style="z-index: 5; display: block; opacity:0;">
	</video>
	<script>
		/* Set video size for each of screens [1920,1280]. Calculations are made in the file main.min.js. */
		!(function(videotag, wrapper) {
			var formats = [
				{
					ext: 'ogv',
					type: "video/ogg; codecs='theora, vorbis'"
				},
				{
					ext: 'mp4',
					type: "video/mp4;"
				},
				{
					ext: 'webm',
					type: "video/webm;"
				}
			];
			$(videotag)
			.tie(function() {
				for (var i=0;i<formats.length;i++) {
					var format = formats[i];
					$(this).put($('<source />', {
						"src": "sleepysleep/video/"+window.videoRatio+"."+format.ext+"?rand="+Math.random(),
						"type": format.type
					}));
				};
			});
			videotag.load();
			
			/* Now we must trim wrapper by aspect ratio 0.5 and wrapper by 0,32291666666666666666666666666667*/
			var trimVideo = function(width) {
				// Trim video
				var wrapH = (parseInt(width)*0.3229);
				var vidH = (parseInt(width)*0.5)+20;
				var vidTop = (wrapH-vidH)/2;
				$(wrapper).css("height", wrapH+'px');
				
				$(videotag).css({
					"height": vidH+'px',
					"margin-top": vidTop+'px'
				});
				// Trim video
			}
			$(window).resize(function() {
				trimVideo($(this).width());
			});
			trimVideo($(window).width());

			/*
			Show video slowly
			*/
			var displayWrapper = function() {
				$(wrapper).animate({
					opacity:1
				},1000);
			}
			var displayVideo = function(ok) {
				displayWrapper();
				if (ok||false) {
					$(videotag).animate({
						opacity:1
					},1000);
					videotag[0].play();
				};
			};

			var testWait, waitLimit, protect = 600;
			var testVideo = function() {	
				protect--;
				if (protect<=0) return;
				if (!waitLimit) return;
				console.log(videotag[0].readyState);
				if (videotag[0].readyState!=4) {
					setTimeout(function() {
						testVideo();
					},50);
				} else {
					
					if (waitLimit) clearTimeout(waitLimit);
					displayVideo(true);
				};
			};
			var waitLimit = setTimeout(function() {
				
				displayVideo();
			}, 750);
			testVideo();

		})($("#header-video"), $("#header-video-wrapper"));
	</script>
	<div class="overlay"></div>
	<img src="sleepysleep/images/banner-logo.png" alt="Sleepeesleep" class="logo" />
</div>


	<div class="products">
	
	<div class="product ">
		<div>
		
		<div class="preview"><img src="sleepysleep/images/product-1.jpg" alt=""></div>
		<div class="descript">
			<h3>матрас  mercury</h3>
			<summary>Матрас премиум класса, собранный в ручную без единой капли клея. Имеет повышенную степень комфорта, поддерживает микроклимат тела.</summary>
			
	<a href="" class="pre-ditails"><button class="ditails">Подробнее</button></a>

		</div>
	
		</div>
	</div>

	
	<div class="product ">
		<div>
		
		<div class="preview"><img src="sleepysleep/images/product-2.jpg" alt=""></div>
		<div class="descript">
			<h3>spring box</h3>
			<summary>Основание Spring Box представляет собой уникальную конструкцию кровати, внутри деревянного каркаса которой содержится независимый пружинный блок. </summary>
			
	<a href="" class="pre-ditails"><button class="ditails">Подробнее</button></a>

		</div>
	
		</div>
	</div>

</div>
	<div class="clear collections">
	
	<div class="collection dotted" id="landscape" style="background-image:url(sleepysleep/images/new-collection-banner.jpg);background-size:auto 100%;height:503px;">
		<div>
			<div class="textblock">
				<table cellpadding="0" cellspacing="0">
					<tobdy>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td class="center">
		<h3>новая коллекция кроватей 2015</h3>
		
	<a href="" class="pre-ditails"><button class="ditails">Подробнее</button></a>

	</td>
						</tr>
						<tr>
							<td></td>
						</tr>
					</tobdy>
				</table>
			</div>
		</div>
	</div>
	
	<div class="collection textured" style="background-image:url(sleepysleep/images/collection-attributes.jpg);background-size:auto 100%;height:598px;">
		<div>
			<div class="textblock">
				<table cellpadding="0" cellspacing="0">
					<tobdy>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td class="center">
		<h3>уникальная коллекция аксессуаров </h3>
		<summary>Прекрасно дополнят интерьер Вашей спальни и придадут уют и комфорт.  Выполненные из натуральных материалов по самым современным дизайнерским решениям.</summary>
		
	<a href=""><button class="ditails">Подробнее</button></a>

	</td>
						</tr>
						<tr>
							<td></td>
						</tr>
					</tobdy>
				</table>
			</div>
		</div>
	</div>

</div>
	<div class="clear">
	
	<div class="collection righter white" style="background-image:url(sleepysleep/images/collection-club.jpg);background-size:auto 100%;background-position:top center;height:542px;">
		<div>
			<div class="textblock">
				<table cellpadding="0" cellspacing="0">
					<tobdy>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td class="center">
								<h3><b>Agalarov Golf  & Country Club</b></h3>
								<summary class="thin">Своим уникальным отношением к комфортной жизни загородное поместье Agalarov EstAgalarov Estate – это концепция, продуманная до мелочей, где созданы все условия для комфортного проживания успешных людей. Въезжая в  “Agalarov Estate” по великолепной сосновой аллее (91 сосна) общей протяженностью 558 м, вы минуете гольф-поле, проезжаете еще  700–800 метров открытого пространства, переезжаете плотину, далее – спортклуб и отель, и лишь после этого начинаются домовладения.</summary>
								
								<a href=""><button class="ditails">Подробнее</button></a>
							</td>
						</tr>
						<tr>
							<td></td>
						</tr>
					</tobdy>
				</table>
			</div>
		</div>
	</div>

	<div class="collection white" id="footer-video-wrapper" style="background-image:url(sleepysleep/images/palladium-banner.jpg);background-size:auto 100%;background-position:top center;height:542px;">
		<video  id="footer-video" loop="" autoplay autoload="autoload" style="opacity:0;">
		</video>
		<script>
			/* Set video size for each of screens [1920,1280]. Calculations are made in the file main.min.js. */
			!(function(videotag, wrapper) {
				var formats = [
					{
						ext: 'ogv',
						type: "video/ogg; codecs='theora, vorbis'"
					},
					{
						ext: 'mp4',
						type: "video/mp4;"
					},
					{
						ext: 'webm',
						type: "video/webm;"
					}
				];
				$(videotag)
				.tie(function() {
					for (var i=0;i<formats.length;i++) {
						var format = formats[i];
						$(this).put($('<source />', {
							"src": "sleepysleep/video/footer/"+window.videoRatio+"."+format.ext+"?rand="+Math.random(),
							"type": format.type
						}));
					};
				});
				videotag.load();
				
				/* Now we must trim wrapper by aspect ratio 0.5 and wrapper by 0,32291666666666666666666666666667*/
				var trimVideo = function(width) {
					// Trim video
					var min = ($(videotag).height()-$(wrapper).height())/2;
					$(videotag).css("margin-top", -min+'px');
					// Trim video
				}
				$(window).resize(function() {
					trimVideo($(this).width());
				});
				trimVideo($(window).width());

				/*
				Show video slowly
				*/
				var displayWrapper = function() {
					$(wrapper).animate({
						opacity:1
					},1000);
				}
				var displayVideo = function(ok) {
					displayWrapper();
					if (ok||false) {
						$(videotag).animate({
							opacity:1
						},1000);
						videotag[0].play();
					};
				};

				var testWait, waitLimit, protect = 600;
				var testVideo = function() {	
					protect--;
					if (protect<=0) return;
					if (!waitLimit) return;
					console.log(videotag[0].readyState);
					if (videotag[0].readyState!=4) {
						setTimeout(function() {
							testVideo();
						},50);
					} else {
						
						if (waitLimit) clearTimeout(waitLimit);
						displayVideo(true);
					};
				};
				var waitLimit = setTimeout(function() {
					
					displayVideo();
				}, 750);
				testVideo();

			})($("#footer-video"), $("#footer-video-wrapper"));
		</script>
		<div>
			<div class="textblock">
				<table cellpadding="0" cellspacing="0">
					<tobdy>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td class="center">
								<h3><img src="sleepysleep/images/palladium-logo.png" /></h3>
								<summary class="thin">Материал, используемый в данном матрасе — сатин, страна производитель - Италия, Stellini. 
Сатин — это натуральная ткань, имеющая особое плотное переплетение нитей, что дает высокую прочность, гладкость и блеск. Благодаря своему составу сатин обладает воздухопроницаемостью и долговечностью. Данный материал используется для изготовления постельного белья с имитацией шелка, поддерживает микроклимат человеческого тела, приятен на ощупь.</summary>
								
								<a href=""><button class="ditails">Подробнее</button></a>
							</td>
						</tr>
						<tr>
							<td></td>
						</tr>
					</tobdy>
				</table>
			</div>
		</div>
	</div>

</div>
	
<div class="limit clear footer">
	<div class="sitemap">
		<ul>
			<li>
				<strong>о компании</strong>
				<ul>
					<li><a href="">Философия</a></li>
					<li><a href="">Технологии</a></li>
					<li><a href="">Производство</a></li>
					<li><a href="">Сертификаты</a></li>
					<li><a href="">Вакансии</a></li>
				</ul>
			</li>
			<li>
				<strong>О нас</strong>
				<ul>
					<li><a href="">Салоны</a></li>
					<li><a href="">Оплата</a></li>
					<li><a href="">Доставка</a></li>
					<li><a href="">Контакты</a></li>
					<li><a href="">Партнерам</a></li>
				</ul>
			</li>
			<li>
				<strong>модели</strong>
				<ul>
					<li><a href="">Двуспальные кровати</a></li>
					<li><a href="">Детские кровати</a></li>
					<li><a href="">spring box</a></li>
					<li><a href="">матрасы премиум</a></li>
					<li><a href="">Беспружинные матрасы</a></li>
					<li><a href="">Все матрасы</a></li>
				</ul>
			</li>
			<li>
				<strong>Аксессуары</strong>
				<ul>
					<li><a href="">Тумбы</a></li>
					<li><a href="">Комоды</a></li>
					<li><a href="">Зеркала</a></li>
					<li><a href="">Банкетки</a></li>
					<li><a href="">Пуфы</a></li>
				</ul>
			</li>
			<li>
				<strong>купить он-лайн</strong>
				<ul>
					<li><a href="">Эл. каталог</a></li>
					<li><a href="">Способы оплаты</a></li>
					<li><a href="">Вопрос-ответ</a></li>
					<li><a href="">Обратная связь</a></li>
				</ul>
			</li>
		</ul>
	</div>
	<div class="copyrights">* 2015 Компания Sleepeesleep - ортопедические матрасы, кровати и аксессуары</div>
</div>


	</body>
<html>