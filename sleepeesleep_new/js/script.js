﻿(function($) {

	$.fn.setHeight = function(callback) {

		var maxHeight,
			that = this,
			resize;

		resize = function() {

			maxHeight = 0;

			that.each(function() {
				this.style.height = "auto";
			});

			that.each(function() {

	            if (maxHeight < $(this).height()) {
	                maxHeight = $(this).height();
	            }
			});

			that.each(function() {

            	$(this).height(maxHeight);

        	});

        	if(callback) {
        		callback(maxHeight);
        	}
		}

		resize();

		$(window).resize(resize);

	}

})(jQuery);

$(window).load(function(){

	$(".js-row").children().setHeight(function(height) {

		$(".js-photo-item-row-2").height(height * 2);

	});

});