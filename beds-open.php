
<!DOCTYPE html>
<html lang="en">
	<head>

		<title></title>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="robots" content="" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

			<script src="sleepysleep/vendor/jquery/dist/jquery.js" type="text/javascript"></script>
			<script src="sleepysleep/vendor/brahma/brahma.js" type="text/javascript"></script>
			<script src="sleepysleep/vendor/brahma.screens/brahma.screens.js" type="text/javascript"></script>
			<script src="sleepysleep/vendor/jquery.tie/jquery.tie.js" type="text/javascript"></script>
			<script src="sleepysleep/js/main.min.js" type="text/javascript"></script>
			<link href="sleepysleep/styles/main.css" rel="stylesheet">
			<link href="sleepysleep/vendor/brahma.screens/brahma.screens.css" rel="stylesheet">
	</head>
	<body class="sleepysleep dark">

	<?
	$superheadersubclass = 'overlay';
	$headersubclass = 'overlay';
	include('.snippets/header.php');?>

	<div class="beds-open" id="screens">
		
			
				<div data-x="0" data-y="0" class="current"><img src="sleepysleep/images/beds/open/slides/1.jpg" alt=""></div>
				<div data-x="1" data-y="0"><img src="sleepysleep/images/beds/open/slides/1.jpg" alt=""></div>
				<div data-x="2" data-y="0"><img src="sleepysleep/images/beds/open/slides/2.jpg" alt=""></div>
				<div data-x="0" data-y="1"><img src="sleepysleep/images/beds/open/slides/3.jpg" alt=""></div>
				<div data-x="1" data-y="1"><img src="sleepysleep/images/beds/open/slides/4.jpg" alt=""></div>
   	            <div data-x="2" data-y="1"><img src="sleepysleep/images/beds/open/slides/4.jpg" alt=""></div>
				<div data-x="0" data-y="2"><img src="sleepysleep/images/beds/open/slides/5.jpg" alt=""></div>
				
		
	</div>
	<div class="beds-ticket">
		<div class="title"><div>SOHO</div></div>

		<div class="descript">
			<div>
				<summary>Современная кровать «Сохо» выглядит мягкой и объемной за счет необычного дизайна стежки. Уникальная стежка образует легкие складки, за счет чего кровать выглядит еще более воздушной. Дизайн кровати безупречен и отражает последние тенденции мировой мебельной моды.</summary>
				<a href="">Купить онлайн</a>
			</div>
		</div>
	</div>
	<script>
		Brahma("#screens").component('screens', {
			infinity:true,
			infinityMethod: 'discover'
		});
	</script>
	</body>
<html>