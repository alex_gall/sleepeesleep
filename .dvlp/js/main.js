/* Video calculates */
;(function(win) {
	var screenWidth = $(win).width();
	
	win.videoRatio = 1280;
	if (screenWidth>1280) {
		win.videoRatio = 1920;
	};
})(window);

/* Parallax effect */
;(function($) {
	var landscape = function(element, options) {
		this.element = element;
		this.options = $.extend({
			motion: 'fixed'
		}, options || {});
		//
		this.info = {
			scrollH: 0,
			totalShift: 0,
			imgH: 0
		};
		// 
		this.objects = {
			image: null
		}
		//
		this.hidden = true;

		this.init = function() {
			var plugin = this;
			// Ожидаем загрузку фонового изображения
			var bg_url = $(this.element).css('background-image');
		    bg_url = /^url\((['"]?)(.*)\1\)$/.exec(bg_url);
		    this.info.bg_url = bg_url ? bg_url[2] : ""; 
		    
		    // Назначаем фон документу
		    this.objects.body = $('body');

		    // Подгружаем фоновое изображение
		    this.objects.image = new Image();
		    this.objects.image.onload = function() {
		    	plugin.readyInit();
		    };
		    this.objects.image.onerror = function() {
		    	plugin.errorInit();
		    };
		    this.objects.image.src = this.info.bg_url;
		}

		this.readyInit = function() {
			var plugin = this;
			// Определяем размер скролла элемента
			this.info.scrollH = $(this.element)[0].scrollHeight;
			
			// Определяем положение элементы по высоте
			this.info.offset = $(this.element).offset();
			// Высота элемента
			this.info.wrapperH = $(this.element).height();
			// Ширина элемента
			this.info.wrapperW = $(this.element).width();
			// Определяем размер самого изображения
			this.info.imageH = this.objects.image.height;
			this.info.imageW = this.objects.image.width;

			// Когда у нас есть размеры изображения, нам необходимо вычеслить его реальные размеры при мастабировании под окно браузера
			var ratio = this.info.wrapperW/this.info.imageW;
			this.info.userImageW = this.info.wrapperW;
			this.info.userImageH = this.info.imageH*ratio;
			if (this.info.userImageH<this.info.wrapperH) {
				this.info.userImageH = this.info.wrapperH;
				var ratio = this.info.wrapperH/this.info.imageH;
				this.info.userImageW = this.info.imageW*ratio;
			};
			// Отрезок картинки, который не влезает в wrapper
			this.info.userImageOverflowY = $(window.top).height()-this.info.userImageH;

			// Переносим изображение на body		
			$(this.objects.body).css({
				'background-repeat': 'no-repeat',
				'background-size': this.info.userImageW+'px '+this.info.userImageH+'px'
			});

			switch(this.options.motion) {
				
				case 'fixed':
					$(this.objects.body).css('background-attachment', 'fixed');
				break;
				default:
					$(this.objects.body).css('background-attachment', 'initial');
				break;
			}

			// Убираем любой фон с контейнера
			$(this.element).css('background', 'transparent');

			// Производим смещение и слушаем события
			this.lookUpScroll();

			$(document).scroll(function() {
				plugin.lookUpScroll();
			});

			$(window).resize(function() {
				plugin.reInit();
			});
		}

		this.reInit = function() {
			var plugin = this;
			// Определяем размер скролла элемента
			this.info.scrollH = $(this.element)[0].scrollHeight;
			
			// Определяем положение элементы по высоте
			this.info.offset = $(this.element).offset();
			// Высота элемента
			this.info.wrapperH = $(this.element).height();
			// Ширина элемента
			this.info.wrapperW = $(this.element).width();
			// Определяем размер самого изображения
			this.info.imageH = this.objects.image.height;
			this.info.imageW = this.objects.image.width;

			// Когда у нас есть размеры изображения, нам необходимо вычеслить его реальные размеры при мастабировании под окно браузера
			var ratio = this.info.wrapperW/this.info.imageW;
			this.info.userImageW = this.info.wrapperW;
			this.info.userImageH = this.info.imageH*ratio;
			if (this.info.userImageH<this.info.wrapperH) {
				this.info.userImageH = this.info.wrapperH;
				var ratio = this.info.wrapperH/this.info.imageH;
				this.info.userImageW = this.info.imageW*ratio;
			};
			// Отрезок картинки, который не влезает в wrapper
			this.info.userImageOverflowY = $(window.top).height()-this.info.userImageH;

			// Переносим изображение на body		
			$(this.objects.body).css({
				'background-size': this.info.userImageW+'px '+this.info.userImageH+'px'
			});

			// Производим смещение и слушаем события
			this.lookUpScroll();
		}

		this.lookUpScroll = function() {

			var scrollTop = $(window).scrollTop();
			var pageHeight = $(window.top).height();

			
			if ((scrollTop+pageHeight) >= this.info.offset.top && scrollTop<=(this.info.offset.top+this.info.wrapperH)) {
				if (this.hidden) this.show();
				
				this.doMove(scrollTop,pageHeight);
			} else {
				if (!this.hidden) this.hide();
			}
		}

		this.doMove = function(scrollTop,pageHeight) {
			// Зная скролл топ и высоту страницы, мы можем вяснить процентный сдвиг фонового изображения
			

			switch (this.options.motion) {
				case 'fixed':
					
					if (this.info.userImageH<pageHeight) {

						var sp = scrollTop+pageHeight;
						var twr = this.info.offset.top+this.info.wrapperH;


						if (sp<twr) {
							this.info.totalShift = (this.info.userImageOverflowY);
						} else if (scrollTop>=this.info.offset.top) {
							this.info.totalShift = 0;
						} else {
							var shiftRatio = (scrollTop+(pageHeight-this.info.wrapperH)-this.info.offset.top)/(pageHeight-this.info.wrapperH);
							
							this.info.totalShift = ((this.info.userImageOverflowY)*(1-shiftRatio));
						}
						$(this.objects.body).css('background-position', '0px '+this.info.totalShift+'px');
					} else {
						$(this.objects.body).css('background-position', '0px 0px');
					}					
					
				break;
				case 'overtake':
					var shiftRatio = ((scrollTop+pageHeight)-this.info.offset.top)/(pageHeight+this.info.wrapperH);
					this.info.totalShift = ((this.info.userImageOverflowY)*(1-shiftRatio));
					
					$(this.objects.body).css('background-position', '0px '+(this.info.totalShift)+'px');
				break;
				case 'classic':
					this.info.totalShift = ((this.info.userImageOverflowY)*(1-shiftRatio));
					
					$(this.objects.body).css('background-position', '0px '+(this.info.totalShift)+'px');
				break;
			}
			
			
			
			
		}

		this.hide = function() {
			$(this.objects.body).css('background-image', 'none');
			this.hidden = true;
		}

		this.show = function() {
			$(this.objects.body).css('background-image', 'url('+this.info.bg_url+')');
			this.hidden = false;
		}

		this.init();
	};

	$.fn.landscape = function(options) {
		
		var options = options || {};
		return $(this).each(function() {
			new landscape(this, options);
		});
	}
})(jQuery);