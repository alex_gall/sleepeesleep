module.exports = function(grunt) {
  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: false,
          yuicompress: true,
          optimization: 2
        },
        files: {
          // target.css file: source.less file
          "../sleepysleep/styles/main.css": "less/main.less"
        }
      }
    },
    uglify: {
      my_target: {
        options: {
          mangle: false
        },
        files: {
          '../sleepysleep/js/main.min.js': ['js/*.js']
        }
      }
    },
    watch: {
      styles: {
        files: ['less/**/*.less','js/*.js'], // which files to watch
        tasks: ['less','uglify'],
        options: {
          nospawn: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');


  grunt.registerTask('default', ['watch', 'less', 'uglify']);
};