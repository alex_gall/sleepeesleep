
<!DOCTYPE html>
<html lang="en">
	<head>

		<title></title>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="robots" content="" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

			<script src="sleepysleep/vendor/jquery/dist/jquery.js" type="text/javascript"></script>
			<script src="sleepysleep/vendor/jquery.tie/jquery.tie.js" type="text/javascript"></script>
			<script src="sleepysleep/js/main.min.js" type="text/javascript"></script>
			<link href="sleepysleep/styles/main.css" rel="stylesheet">
	</head>
	<body class="sleepysleep">

	<?
	$superheadersubclass = 'overlay';
	$headersubclass = 'overlay';
	include('.snippets/header.php');?>
	<div class="submatresses-under" id="m-under">
		<div id="rowitems">
			<div rel="id1" style="background-image: url(sleepysleep/images/matrasses/sub-under/bg/1.jpg)">
				<img src="sleepysleep/images/matrasses/sub-under/grad.png" alt="">
				<div>
					<article>
						<h4>Cерия HILTON</h4>
						<summary><span>Серия, созданная для людей ценях комфорт, роскошь и индивидуальность. Независимый пружинный блок обеспечивает ей идеальную ортопедическую поддержку позвоночника.</span></summary>
					</article>
				</div>
			</div>
			<div rel="id1" style="background-image: url(sleepysleep/images/matrasses/sub-under/bg/2.jpg)">
				<img src="sleepysleep/images/matrasses/sub-under/grad.png" alt="">
				<div>
					<article>
						<h4>Серия SAVOY</h4>
						<summary><span>Создана для любителей матрасов  с повышенным жесткости. В наполнении используются преимущественно натуральные и гипоаллергенные материалы. </span></summary>
					</article>
				</div>
			</div>
			<div rel="id1" style="background-image: url(sleepysleep/images/matrasses/sub-under/bg/1.jpg)">
				<img src="sleepysleep/images/matrasses/sub-under/grad.png" alt="">
				<div>
					<article>
						<h4>Серия ROYAL</h4>
						<summary><span>Матрас полностью оправдывает свое название благодаря внешнему виду и безупречному наполнению. Модели Royal подойдут для людей, которые ценят особый комфорт и уютную мягкость в спальном ложе.</span></summary>
					</article>
				</div>
			</div>
			<div rel="id1" style="background-image: url(sleepysleep/images/matrasses/sub-under/bg/2.jpg)">
				<img src="sleepysleep/images/matrasses/sub-under/grad.png" alt="">
				<div>
					<article>
						<h4>Серия CHELSEA</h4>
						<summary><span>Серия матрасов Chelsea - это практичные, надежные матрасы. В основе этих матрасов лежит независимый пружинный блок, который обеспечивает поддержку позвоночника.</span></summary>
					</article>
				</div>
			</div>
		</div>
		<script>
			
		;(function() {


			var items = $("#rowitems>* summary");
			var recalc = function() {
				/* Высота колонок под размер экрана*/
				$("#m-under").css("height", $(window).height());
				/* Уравновешиваем строки */
				var max = 0;
				$(items).css("height","auto");
				$(items).each(function() {
					var h = $(this).height();
					(h>max) && (max=h);
				});
				$(items).css("height", max+'px')
			}
			recalc();
			$(window).resize(function() {
				recalc();
			});
		})();
		</script>
	</div>
<div class="limit clear footer dark">
	<article>Sleepeesleep – бренд группы компаний Estetica, признанного одним из лучших производителей ортопедических матрасов и входящего в тройку крупнейших российских производителей товаров для сна. Вся продукция создается из сертифицированных гипоаллергенных материалов.</article>
	<div class="copyrights-pro">
		<a href=""><img src="sleepysleep/images/fsoc_vk.png" alt="vk" /></a>
		<a href=""><img src="sleepysleep/images/fsoc_tw.png" alt="vk" /></a>
		<a href=""><img src="sleepysleep/images/fsoc_fb.png" alt="vk" /></a>
		<summary>© 2015 группа компаний «эстетика»</summary>
	</div>
</div>


	</body>
<html>