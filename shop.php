<!DOCTYPE html>
<html lang="en">
<head>
	<title>Shop</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script src="sleepysleep/vendor/jquery/dist/jquery.js" type="text/javascript"></script>
	<script src="sleepysleep/vendor/jquery.tie/jquery.tie.js" type="text/javascript"></script>
	<script src="sleepysleep/js/shop.js" type="text/javascript"></script>
	<link href="sleepysleep/styles/main.css" rel="stylesheet">
</head>
<body class="sleepysleep">
	<table class="shop-mode">
		<tbody>
			<tr>
				<td id="contents">
					<div class="shop">
						<div class="container">
							<div class="gallery" id="gallery">
								<div>
									<table>
										<tbody>
											<tr>
												<td class="y0x0" data-title="Салоны" data-subscript="Подробнее">
													<img src="sleepysleep/images/shop/gallery/y0x0.jpg" alt="">
												</td>
												<td class="y0x1" data-title="Салоны" data-subscript="Подробнее">
													<img src="sleepysleep/images/shop/gallery/y0x1.jpg" alt="">
												</td>
											</tr>
										</tbody>
									</table>
									<table>
										<tbody>
											<tr>
												<td class="y1x0" data-title="Салоны" data-subscript="Подробнее">
													<img src="sleepysleep/images/shop/gallery/y1x0.jpg" alt="">
												</td>
												<td class="y1x1" data-title="Салоны" data-subscript="Подробнее">
													<img src="sleepysleep/images/shop/gallery/y1x1.jpg" alt="">
												</td>
												<td class="y1x2" data-title="Салоны" data-subscript="Подробнее">
													<img src="sleepysleep/images/shop/gallery/y1x2.jpg" alt="" valign="bottom">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="sidebar">
								<div class="push-v-header">
									<a href="" class="upback-trigger"><span>Возврат</span></a>
								</div>
								<div class="push-v-center">
									<div class="logo">
										<img src="sleepysleep/images/shop/logo.png" alt="">
									</div>
									<div class="menu">
										<ul>
											<li><a href="">Акции</a></li>
											<li><a href="">Кровати</a></li>
											<li><a href="" data-trigger="show-submenu">Матрасы</a></li>
											<li><a href="">Подбор матрасов</a></li>
											<li><a href="">Салоны</a></li>
										</ul>
									</div>
								</div>
								<div class="push-v-footer">
									<div class="phone">8 800 500
										<noskype></noskype> 54 45</div>
									<div class="phone-comment">Заказать обратный звонок</div>
									<div class="soc">
										<a href=""><img src="sleepysleep/images/shop/soc-vk.png" alt="ВКонтакте"></a>
										<a href=""><img src="sleepysleep/images/shop/soc-twit.png" alt="Twitter"></a>
										<a href=""><img src="sleepysleep/images/shop/soc-fb.png" alt="Facebook"></a>
									</div>
								</div>
								<div class="submenu" id="submenu">
									<div class="second" id="submenu-second">
										<div>
											<h5>Односпальные</h5>
											<ul>
												<li><a href="">80x190</a></li>
												<li><a href="">80x195</a></li>
												<li><a href="">80x200</a></li>
												<li><a href="">90x200</a></li>
												<li><a href="">90x190</a></li>
												<li><a href="">90x195</a></li>
												<li><a href="">90x200</a></li>
											</ul>
											<h5>Полуторные</h5>
											<ul>
												<li><a href="">120x190</a></li>
												<li><a href="">120x195</a></li>
												<li><a href="">120x200</a></li>
												<li><a href="">120x200</a></li>
												<li><a href="">120x190</a></li>
												<li><a href="">120x195</a></li>
												<li><a href="">120x200</a></li>
											</ul>
											<h5>Двухспальные</h5>
											<ul>
												<li><a href="">160x190</a></li>
												<li><a href="">160x195</a></li>
												<li><a href="">160x200</a></li>
												<li><a href="">180x200</a></li>
												<li><a href="">180x190</a></li>
												<li><a href="">180x195</a></li>
												<li><a href="">180x200</a></li>
												<li><a href="">200x190</a></li>
												<li><a href="">200x195</a></li>
												<li><a href="">200x200</a></li>
											</ul>
										</div>
									</div>
									<div class="first" id="submenu-first">
										<div>
											<h4>Все модели</h4>
											<ul>
												<li><a href="">Беспружинные</a></li>
												<li><a href="">Мягкие матрасы</a></li>
												<li><a href="">Жесткие матрасы</a></li>
												<li><a href="">Двухсторонние</a></li>
												<li><a href="">Детские</a></li>
												<li><a href="">Наматрасники</a></li>
											</ul>
											<h4>Детские</h4>
										</div>
									</div>
								</div>
								<script>
								subMenuController();
								</script>
							</div>
							<script>
							$("#gallery").shopGallery();
							</script>
							<div class="topbar">
								<div>
									<div class="contacts">
										<div><a href="">Заказать звонок</a></div>
										<div>
											<a href="mailto:"><img src="sleepysleep/images/shop/topbar-mail.png" alt="Эл. почта"></a>
										</div>
										<div><span>8 800 500 54 45</span></div>
									</div>
									<div class="cart-block">
										<div class="cart">
											<label for="">Корзина</label>
											<input type="text" value="1" readonly="readonly">
										</div>
										<a href="">
											<button>Оформить заказ</button>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="limit clear footer" id="footer">
						<article>Sleepeesleep – бренд группы компаний Estetica, признанного одним из лучших производителей ортопедических матрасов и входящего в тройку крупнейших российских производителей товаров для сна. Вся продукция создается из сертифицированных гипоаллергенных материалов.</article>
						<div class="copyrights-pro">
							<a href=""><img src="sleepysleep/images/fsoc_vk.png" alt="vk" /></a>
							<a href=""><img src="sleepysleep/images/fsoc_tw.png" alt="vk" /></a>
							<a href=""><img src="sleepysleep/images/fsoc_fb.png" alt="vk" /></a>
							<summary>© 2015 группа компаний «эстетика»</summary>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	
	<script>
	// Fix for IE
	if (true)
	{
		!(function()
		{
			var footerHeight = $("#footer").outerHeight();
			var shop = $("#contents")[0];
			var chopShop = function()
			{
				shop.style.height = parseInt($(window).height()) + footerHeight + 'px';
				window.refreshShop();
			};
			$(window).resize(chopShop);
			chopShop();
		})();
	};
	</script>
	<script>
	window.show1 = function()
	{
		$("#submenu").removeClass("show-second").addClass("show-first");
	};
	window.show2 = function()
	{
		$("#submenu").removeClass("show-first").addClass("show-second");
	};
	</script>
</body>
<html>