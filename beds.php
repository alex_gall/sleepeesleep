
<!DOCTYPE html>
<html lang="en">
	<head>

		<title></title>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="robots" content="" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

			<script src="sleepysleep/vendor/jquery/dist/jquery.js" type="text/javascript"></script>
			<script src="sleepysleep/vendor/jquery.tie/jquery.tie.js" type="text/javascript"></script>
			<script src="sleepysleep/js/main.min.js" type="text/javascript"></script>
			<link href="sleepysleep/styles/main.css" rel="stylesheet">
	</head>
	<body class="sleepysleep">

	
	<?include('.snippets/header.php');?>
<div class="matrasses-gallery" id="gallery-area">
	<img src="sleepysleep/images/beds/bg/0.jpg" class="current" id="bg0" alt="">
	<table class="view">
		<tbody>
			<tr class="tb">
				<td></td>
			</tr>
			<tr class="center">
				<td>
					<div class="cells fillall" id="cells">
						<div class="fill"><a href="" data-bg="1">
							<span>Линейка стеганых кроватей</span>
						</a></div>
						<div><a href="" data-bg="2"><span>Линейка дизайнерских кроватей</span></a></div>
						<div><a href="" data-bg="3"><span>Детские кровати</span></a></div>
						<div><a href="" data-bg="4">
							<img src="sleepysleep/images/beds/springbox.png" />
						</a></div>
					</div>
				</td>
			</tr>
			<tr class="tb">
				<td></td>
			</tr>
		</tbody>
	</table>
	<table class="start">
		<tbody>
			<tr class="tb">
				<td></td>
			</tr>
			<tr class="center">
				<td>
					<div class="cells">
						<div class="fill">
							<a href="">
								<img src="sleepysleep/images/sleepeesleep-logo-medium.png" />
							</a>
							<div class="loadline" id="ll"><div></div></div>
						</div>
					</div>
				</td>
			</tr>
			<tr class="tb">
				<td></td>
			</tr>
		</tbody>
	</table>
</div>
<script>
	

	// Present effect
	new (function(start, view) {
		this.done = false;
		this.ready = false;
		this.loadings = 0;
		this.maxloadings = 0;
		this.released = 0;
		this.toload = [
			'sleepysleep/images/beds/bg/4.jpg',
			'sleepysleep/images/beds/bg/2.jpg',
			'sleepysleep/images/beds/bg/3.jpg',
			'sleepysleep/images/beds/bg/1.jpg',
		];
		this.initial = function() {
			this.ll = $("#ll>div");

			var that = this;

			this.loadings = this.toload.length;

			for (var i=0;i<this.toload.length;i++) {
				;(function(index, src) {
					var img = new Image();
					img.onload = function() {
						that.loaded(index);
					}
					img.src = src;
				})(i, this.toload[i]);
			};

			//
			setTimeout(function() {
				that.tryRelease();
			}, 1000);
		}
		this.loaded = function(index) {
			if (this.done===true) return;
			this.loadings--;

			var v = (100-(100*(this.loadings/this.toload.length) ));
			$(this.ll).css('width', v+'%');

			$('<img />', {
				"src": this.toload[index],
				"id": "bg"+(index+1)
			}).insertAfter($("#bg0"));

			if (this.loadings===0) {
				this.done = true;
				this.tryRelease();
			}
		};
		this.tryRelease = function() {
			if (this.ready) {
				this.release();
			} else {
				this.ready = true;
			}
		};
		this.release = function() {

			$(start).find('.cells').addClass('go').animate({
				width:"756px",
				height:"365px"
			},1000, function() {
				$(view).show();
				$(start).hide();
			});

			// Init blocks
			new (function(c, g) {
				var all = $("#gallery-area>img");
				var alll = $("#cells>div");
				$(c).find('>div>a').each(function() {
					var bgnode = $("#bg"+$(this).data("bg"));
					$(this).hover(function() {
						$(all).removeClass('current');
						$(alll).removeClass('fill');
						$(bgnode).addClass('current');
						$(this).parent().addClass('fill');
						return false;
					});
			});
		
		
	})($("#cells"), $("#gallery-area"));
			
		}

		this.initial();
		
	})($('table.start'),$('table.view'));

</script>
<div class="limit clear footer">
	<article>Sleepeesleep – бренд группы компаний Estetica, признанного одним из лучших производителей ортопедических матрасов и входящего в тройку крупнейших российских производителей товаров для сна. Вся продукция создается из сертифицированных гипоаллергенных материалов.</article>
	<div class="copyrights-pro">
		<a href=""><img src="sleepysleep/images/fsoc_vk.png" alt="vk" /></a>
		<a href=""><img src="sleepysleep/images/fsoc_tw.png" alt="vk" /></a>
		<a href=""><img src="sleepysleep/images/fsoc_fb.png" alt="vk" /></a>
		<summary>© 2015 группа компаний «эстетика»</summary>
	</div>
</div>


	</body>
<html>