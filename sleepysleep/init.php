<?include($_SERVER['DOCUMENT_ROOT'].'/.dharma/suit.php');?>
<doctype>html:5</doctype>
<!--Require jQuery-->
<bower package="jquery"></bower>
<bower package="jquery.tie"></bower>

<!--Connect styles-->
<css>/@/styles/main.css</css>

<element name="limitbox">
<div class="limit [[%.class]]">[[content]]</div>
</element>

<element name="product">
	<div class="product [[%.class]]">
		<div>
		[[content]]
		</div>
	</div>
</element>

<element name="collection">
	<div class="collection [[%.class]]" style="[[%.style]]">
		<div>
			<div class="textblock">
				<table cellpadding="0" cellspacing="0">
					<tobdy>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td class="center">[[content]]</td>
						</tr>
						<tr>
							<td></td>
						</tr>
					</tobdy>
				</table>
			</div>
		</div>
	</div>
</element>

<element name="button-ditails">
	<button class="ditails">Подробнее</button>
</element>

<!--Connect scripts-->
<script src="/@/js/main.min.js"></script>
<body class="sleepysleep">[[contents]]</body>